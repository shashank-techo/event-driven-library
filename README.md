# Event Driven Library

### Use case
A sample library and its implementation for implementing event-driven system using solace event broker.

### Contents
1. [event-driven-lib](https://gitlab.com/shashank-techo/event-driven-library/-/tree/master/event-driven-lib) (library created using jcsmp library of solace and uses tcp connection)
2. [microservice](https://gitlab.com/shashank-techo/event-driven-library/-/tree/master/microservice) (sample Spring boot service)
3. [mqtt](https://gitlab.com/shashank-techo/event-driven-library/-/tree/master/mqtt) (a sample project to showcase publishing & subscribing of messages using a mqtt connection to a solace broker)

### Dependencies 
1. In the pom.xml of [event-driven-lib](https://gitlab.com/shashank-techo/event-driven-library/-/blob/master/event-driven-lib/pom.xml), dependency for solace jcsmp.
```maven
		<dependency>
			<groupId>com.solacesystems</groupId>
			<artifactId>sol-jcsmp</artifactId>
			<version>10.9.1</version>
		</dependency>
```

2. In the pom.xml of [mqtt](https://gitlab.com/shashank-techo/event-driven-library/-/blob/master/mqtt/pom.xml), dependency for paho library which has support for mqtt client.
```maven
		<dependency>
			<groupId>org.eclipse.paho</groupId>
			<artifactId>org.eclipse.paho.client.mqttv3</artifactId>
			<version>1.2.0</version>
		</dependency>
```
> :warning: It is advised not to change the version of paho library as some other versions are unstable.

### Notes for configuration 
1. In order to use [event-driven-lib](https://gitlab.com/shashank-techo/event-driven-library/-/tree/master/event-driven-lib), add a dependency of the jar of this library in your project's pom.xml. Refer[sample project's pom.xml](https://gitlab.com/shashank-techo/event-driven-library/-/blob/master/microservice/pom.xml)
Ex: 
```maven
		<dependency>
			<groupId>id.co.xl.techolution</groupId>
			<artifactId>event-driven-lib</artifactId>
			<version>1.0.0</version>
		</dependency>
```

2. Add connection configuration in your application.properties file. Refer [application.properties](https://gitlab.com/shashank-techo/event-driven-library/-/blob/master/microservice/src/main/resources/application.properties)
```
    solace.vpn = vpn-name
    solace.host = localhost:55443
    solace.username = clientname
    solace.password = password
    solace.java.connectRetries=5
    solace.java.reconnectRetries=5
    solace.java.reconnectRetryWaitInMillis=500

```

3. Configure a subscriber for your topic and queue. Refer [SolaceListener.java](https://gitlab.com/shashank-techo/event-driven-library/-/blob/master/microservice/src/main/java/com/example/microservice/listeners/SolaceListener.java) for topic listener and [SolaceQueueListener.java](https://gitlab.com/shashank-techo/event-driven-library/-/blob/master/microservice/src/main/java/com/example/microservice/listeners/SolaceQueueListener.java) for queue listener

4. Implement callback handler methods for by refering [QueuePublisherCallback.java](https://gitlab.com/shashank-techo/event-driven-library/-/blob/master/microservice/src/main/java/com/example/microservice/callbacks/QueuePublisherCallback.java) for queue and [TopicPublisherCallback.java](https://gitlab.com/shashank-techo/event-driven-library/-/blob/master/microservice/src/main/java/com/example/microservice/callbacks/TopicPublisherCallback.java) for topics.

5. Publish your message to a queue or topic. Refer [QueueController.java](https://gitlab.com/shashank-techo/event-driven-library/-/blob/master/microservice/src/main/java/com/example/microservice/controller/QueueController.java) for queue and [TopicController.java](https://gitlab.com/shashank-techo/event-driven-library/-/blob/master/microservice/src/main/java/com/example/microservice/controller/TopicController.java) for topic.


### Notes for mqtt client configuration 
1. Add paho library dependency in your project
```maven
		<dependency>
			<groupId>org.eclipse.paho</groupId>
			<artifactId>org.eclipse.paho.client.mqttv3</artifactId>
			<version>1.2.0</version>
		</dependency>
```
Refer [pom.xml](https://gitlab.com/shashank-techo/event-driven-library/-/blob/master/mqtt/pom.xml) of [mqtt](https://gitlab.com/shashank-techo/event-driven-library/-/tree/master/mqtt).

2.Add connection configuration in your application.properties file. Refer [application.properties](https://gitlab.com/shashank-techo/event-driven-library/-/blob/master/mqtt/src/main/resources/application.properties)
```
mqttusername = solace-cloud-client
mqtthost = wss://mr14l2q28lvwjt.messaging.solace.cloud:8443
mqttpassword = oqmul72m6u949n4hlirq3uvnpl

```

3. Refer [MQTTClientConfig.java](https://gitlab.com/shashank-techo/event-driven-library/-/blob/master/mqtt/src/main/java/com/sample/solace/mqtt/config/MQTTClientConfig.java) which is responsible for creating a client.

4. Add a listener/subscriber(which runs in a background thread) to your project in order to listen to a topic. Refer [MqttSubscriber.java](https://gitlab.com/shashank-techo/event-driven-library/-/blob/master/mqtt/src/main/java/com/sample/solace/mqtt/listener/MqttSubscriber.java).

5. Publish a message to a topic. Refer [TopicService.java](https://gitlab.com/shashank-techo/event-driven-library/-/blob/master/mqtt/src/main/java/com/sample/solace/mqtt/service/TopicService.java).




### References
1. You can refer to [solace-samples-java](https://github.com/SolaceSamples/solace-samples-java) and [solace-samples-mqtt](https://github.com/SolaceSamples/solace-samples-mqtt) for future references.